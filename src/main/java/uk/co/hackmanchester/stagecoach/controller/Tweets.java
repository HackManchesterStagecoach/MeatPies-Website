package uk.co.hackmanchester.stagecoach.controller;

public class Tweets {
	
	private long id;
	
	private String context;
	
	private String user;

	private long tweetId;

	public Tweets(long id, long tweetId, String context, String user) {
		this.user = user;
		this.context = context;
		this.tweetId = tweetId;
		this.id = id;
	}

	public Tweets(long tweetId, String context, String user) {
		this.user = user;
		this.context = context;
		this.tweetId = tweetId;
	}

	public long getId() {
		return id;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public long getTweetId() {
		return tweetId;
	}

	public void setTweetId(long tweetId) {
		this.tweetId = tweetId;
	}

	@Override
	public String toString() {
		return "Tweets [id=" + id + ", context=" + context + ", user=" + user + ", tweetId=" + tweetId + "]";
	}

	
}
