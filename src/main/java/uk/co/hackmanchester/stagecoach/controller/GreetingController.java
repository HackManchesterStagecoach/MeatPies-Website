package uk.co.hackmanchester.stagecoach.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

	@Autowired
	private StoreService storeService;

	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String greeting(@RequestParam(value = "tweetId", required=true) Long tweetId, @RequestParam(value = "user", required=true) String user, Model model){
		model.addAttribute("tweetId", tweetId);
		model.addAttribute("user", user);
		boolean noError = storeService.fetchTweetAndStore(new Tweets(tweetId, "", user));
		model.addAttribute("noError", noError);
		return "new";
	}

    @RequestMapping("/")
    public String showBlog(@RequestParam(value = "limit", required=false, defaultValue = "100") Integer limit, @RequestParam(value = "offset", required=false, defaultValue = "-1") Integer offset, Model model){
        try {
            List<Tweets> listOfTweets = storeService.findLatestTweets(limit,offset);
            storeService.convertTweets(listOfTweets);
            model.addAttribute("tweets", listOfTweets);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "blog";
    }

}
