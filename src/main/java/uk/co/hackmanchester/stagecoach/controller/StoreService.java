package uk.co.hackmanchester.stagecoach.controller;

import java.util.List;

public interface StoreService {

	void save(Tweets tweets);
	
	List<Tweets> findLatestTweets(Integer limit, Integer offset);

	boolean fetchTweetAndStore(Tweets tweet);

	List<String> convertTweets(List<Tweets> listOfTweets);

	void update(Tweets tweet);
}
