package uk.co.hackmanchester.stagecoach.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

@Service
public class DefaultStoreService implements StoreService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private String insertSql = "INSERT INTO tweets (context, user_id, tweet_id) VALUES (?, ?, ?)";

	private String latestSequence = "select id, context, user_id, tweet_id from tweets order by id desc limit ";
	@Override
	public void save(Tweets tweet) {
		jdbcTemplate.update(insertSql, new Object[] { tweet.getContext(), tweet.getUser(), tweet.getTweetId() });
	}

	@Override
	public List<String> convertTweets(List<Tweets> listOfTweets) {
		Gson gson = new Gson();
		final HttpClient client = HttpClientBuilder.create().build();
		List<String> tweetHtml = new ArrayList<>();
		for (Tweets tweet : listOfTweets) {
			if (tweet.getContext() != null) {
				tweetHtml.add(tweet.getContext());
			} else {
				TwitterEmbed embed = getTweetAndSetContext(tweet, gson, client);
				tweetHtml.add(embed.getHtml());
			}
		}
		return tweetHtml;
	}


	private String getJson(HttpResponse response) throws IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuilder result = new StringBuilder();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		return result.toString();
	}

	@Override
	public boolean fetchTweetAndStore(Tweets tweet) {
		Gson gson = new Gson();
		final HttpClient client = HttpClientBuilder.create().build();
		TwitterEmbed embed = getTweetAndSetContext(tweet, gson, client);
		if (embed == null){
			return false;
		}
		
		save(tweet);
		return true;
	}

	private TwitterEmbed getTweetAndSetContext(Tweets tweet, Gson gson, final HttpClient client) {
		HttpGet getter = new HttpGet("https://api.twitter.com/1.1/statuses/oembed.json?id=" + tweet.getTweetId());
		try {
			HttpResponse response = client.execute(getter);
			if (response.getStatusLine().getStatusCode() != 200){
				return null;
			}
			String json = getJson(response);
			TwitterEmbed embed = gson.fromJson(json, TwitterEmbed.class);
			tweet.setContext(embed.getHtml());
			return embed;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

    public class TweetRowMapper implements RowMapper
    {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Tweets(rs.getLong("id"),
                    rs.getLong("tweet_id"),
                    rs.getString("context"),
                    rs.getString("user_id"));
        }

    }

    @Override
    public List<Tweets> findLatestTweets(Integer limit, Integer offset) {
        if (offset < 0) {
            return jdbcTemplate.query(latestSequence + limit, new TweetRowMapper());
        }
        return jdbcTemplate.query( "select id, context, user_id, tweet_id from tweets where id >= " + offset +
                " and id <= " + (limit + offset) + " order by id desc limit " + limit, new TweetRowMapper());
    }

    @Override
    public void update(Tweets tweet) {
        jdbcTemplate.execute("update tweets set context = '" + tweet.getContext() + "' where id = " + tweet.getId());
    }


}
